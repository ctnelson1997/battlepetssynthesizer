'use strict';

const fs = require('fs');
const path = require('path');
const minimist = require('minimist');
const replace = require("replace");

var argv = minimist(process.argv.slice(2));

let CREATIONS_ROOT_PATH = argv.creations_path.replace(/\/?$/, '/');
let SCENARIOS_PATH = CREATIONS_ROOT_PATH + 'scenarios/';
let PETS_PATH = CREATIONS_ROOT_PATH + 'pets/';
let GENETICS_PATH = CREATIONS_ROOT_PATH + 'genetics/';

let GAME_ROOT_PATH = argv.game_path.replace(/\/?$/, '/');
let PLAYABLES_PATH = GAME_ROOT_PATH + "edu/dselent/player/";

let INPUT_FILE_PATH = argv.input_path || GAME_ROOT_PATH;

let CHOSEN_SCENARIO_ID = argv.scenario_id;

let scenarios = [];
let pets = [];

getAllFiles(SCENARIOS_PATH).forEach(file => {
    let scenario = JSON.parse(fs.readFileSync(file));
    scenario.id = file.substr(SCENARIOS_PATH.length);
    scenarios.push(scenario);
});

getAllFiles(PETS_PATH).forEach(file => {
    let pet = JSON.parse(fs.readFileSync(file));
    pet.id = file.substr(PETS_PATH.length);
    pets.push(pet);
});

console.log();
console.log("SCENARIOS");
console.log("-------------------------");
scenarios.forEach(scenario => {
    console.log(scenario)
});

console.log();
console.log("PETS");
console.log("-------------------------");
pets.forEach(pet => {
    console.log(pet)
});

console.log("Searching for scenario " + CHOSEN_SCENARIO_ID);

let choosen_scenario = undefined;
scenarios.forEach(scenario => {
    if(scenario.id.valueOf() === CHOSEN_SCENARIO_ID.valueOf()) {
        choosen_scenario = scenario;
        console.log("Found a match of scenario id " + CHOSEN_SCENARIO_ID);
    }
});

if(choosen_scenario == undefined)
    throw new Error("Could not find a scenario of the id " + CHOSEN_SCENARIO_ID + "! Is the path specified correctly?");
else
    console.log("Successfully found the scenario!");

console.log("Searching for pets " + choosen_scenario.pets);

let choosen_pets = [];
choosen_scenario.pets.forEach(pet_id => {
    let found_pet = pets.find(pet => {
        return (pet_id.valueOf() === pet.id.valueOf());
    })
    if(found_pet == null || found_pet == undefined)
        throw new Error("Could not find pet id matching " + pet_id + "! Is the path specified correctly?");
    else
        console.log("Found a match of pet id " + pet_id);
    console.log("Adding " + found_pet)
    choosen_pets.push(found_pet);
});

if(choosen_pets.length != choosen_scenario.pets.length)
    throw new Error("Could not find all pets for the scenario! Are the paths specified correctly?");
else
    console.log("Successfully found all pets!");

console.log("Beginning injection code for PlayerTypes.java")
let enum_genetics = "";
choosen_scenario.pets.forEach((pet, index) => {
    enum_genetics += ",\n\tCUSTOM" + index;
});
enum_genetics += ";";
console.log("Injection code finished for PlayerTypes.java.");
console.log("-------------------------");
console.log(enum_genetics);
console.log("-------------------------");

console.log("Beginning injection code for PlayableInstantiator.java")
let elif_blocks = "";
choosen_pets.forEach((pet, index) => {
    console.log(pet);
    elif_blocks += "else if(playerType == PlayerTypes.CUSTOM" + index + ")\n" +
        "\t\t{\n" +
        "\t\t\tthePlayable = new " + pet.genetic_code.split(".")[0] + "(playableUid, playerInfo);\n" +
        "\t\t}"
});
console.log("Injection code finished for PlayableInstantiator.");
console.log("-------------------------");
console.log(elif_blocks);
console.log("-------------------------");


console.log("Beginning injection to PlayerTypes.java...");
replace({
    regex: ";\\s*\/\/ Add more here",
    replacement: enum_genetics,
    paths: [PLAYABLES_PATH + 'PlayerTypes.java'],
    recursive: true,
    silent: true,
});
console.log("Injection finished to PlayerTypes.java.");

console.log("Beginning injection to PlayableInstantiator.java...");
replace({
    regex: "..\\s*Add more here\\s*..",
    replacement: elif_blocks,
    paths: [PLAYABLES_PATH + 'PlayableInstantiator.java'],
    recursive: true,
    silent: true,
});
console.log("Injection finished to PlayableInstantiator.java.");


let used_genetics = [];
pets.forEach(pet => {
    if(used_genetics.includes(pet.genetic_code))
        console.log("Already have genetic code " + pet.genetic_code + ", ignoring.");
    else
    {
        used_genetics.push(pet.genetic_code);
        console.log("Found new genetic code " + pet.genetic_code + ", adding.");
    }
});

console.log("Moving genetic code into codebase.");
used_genetics.forEach(genetic_code => {
    fs.copyFileSync(GENETICS_PATH + genetic_code, PLAYABLES_PATH + genetic_code, (err) => {
        if (err) throw err;
        console.log('Copied ' + GENETICS_PATH + genetic_code + ' to ' + PLAYABLES_PATH + genetic_code);
    });
});
console.log("Done moving genetic code!");


let input_data = "";
console.log("Generating input data...");
input_data += choosen_scenario.seed + "\n";
input_data += choosen_scenario.pets.length + "\n";
input_data += choosen_scenario.number_of_fights + "\n";
choosen_pets.forEach((pet, i) => {
    input_data += (i + 3) + "\n";
    if(pet.pet_type.toUpperCase() === "POWER" || pet.pet_type.toUpperCase() === "P" || pet.pet_type.toUpperCase() === "1")
        input_data += "1\n";
    else if(pet.pet_type.toUpperCase() === "SPEED" || pet.pet_type.toUpperCase() === "S" || pet.pet_type.toUpperCase() === "2")
        input_data += "2\n";
    else if(pet.pet_type.toUpperCase() === "INTELLIGENCE" || pet.pet_type.toUpperCase() === "INTELLIGENT" || pet.pet_type.toUpperCase() === "I" || pet.pet_type.toUpperCase() === "3")
        input_data += "3\n";
    else
        throw  new Error(pet.pet_type + " is not a valid type of pet (power, speed, intelligence)");
    input_data += pet.owner_name + "\n";
    input_data += pet.pet_name + "\n";
    input_data += pet.health + "\n";
});
console.log("Generated input data.");
console.log("-------------------------");
console.log(input_data);
console.log("-------------------------");

console.log("Moving input data to " + INPUT_FILE_PATH);
fs.writeFileSync(INPUT_FILE_PATH + 'input.in', input_data, function(err) {
    if(err) throw err;
    console.log("Saved input data to " + INPUT_FILE_PATH);
});

console.log("Done!")

function getAllFiles(dir) {
    let files = [];
    traverseDir(dir, files);
    return files;
}

// adapted from https://stackoverflow.com/questions/50121881/node-js-recursively-list-full-path-of-files
function traverseDir(dir, files) {
    fs.readdirSync(dir).forEach(file => {
        let fullPath = path.join(dir, file);
        if (fs.lstatSync(fullPath).isDirectory()) {
            traverseDir(fullPath, files);
        } else {
            files.push(fullPath);
        }
    });
}